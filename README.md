# fugue-steps

Library to allow for-comprehension style syntax within Java8+

Works for io.atlassian.fugue Option and Either, as well as Java8 Optional

```
    Either<AnError, Double> either = Steps
                    .begin(firstOKOperation())
                    .then(str -> secondNGOperation(str))
                    .then((str, number) -> thirdOKOperation(number))
                    .then((str, number, boo) -> fourthOKOperation(str, number, boo))
                    .then(() -> fifthOKOperation())
                    .yield((str, number, boo, str2, fifth) -> new Double(number));
```

```
    Option<Double> either = Steps
                    .begin(firstOKOperation())
                    .then(str -> secondNGOperation(str))
                    .then(() -> thirdOKOperation(number))
                    .then((str, number, boo) -> fourthOKOperation(str, number, boo))
                    .then((str, number, boo, str2) -> fifthOKOperation())
                    .yield((str, number, boo, str2, fifth) -> new Double(number));
```

```
    Optional<Integer> either = Steps
                    .begin(firstOKOperation())
                    .then(() -> secondNGOperation(str))
                    .then((str, number) -> thirdOKOperation(number))
                    .then((str, number, boo) -> fourthOKOperation(str, number, boo))
                    .then((str, number, boo, str2) -> fifthOKOperation())
                    .then((str, number, boo, str2, fifth) -> sixthOKOperation())
                    .yield((str, number, boo, str2, fifth, sixthInt) -> sixthInt);
```

Will work up to 6 Steps at the moment.

Will break out on the first Left (Either), none (Option) or empty (Optional)