package com.mmcmahon.fugue.step;

import com.mmcmahon.fugue.step.functions.Function1;
import com.mmcmahon.fugue.step.ops.OptionStep;
import io.atlassian.fugue.Option;

import java.util.function.Supplier;

public class OptionStep1<A> implements OptionStep {

    private final Option<A> option1;

    OptionStep1(Option<A> option1) {
        this.option1 = option1;
    }

    public <B> OptionStep2<A, B> then(Function1<A, Option<B>> functor) {
        Option<B> option2 = option1.flatMap(functor::apply);
        return new OptionStep2<>(option1, option2);
    }

    public <B> OptionStep2<A, B> then(Supplier<Option<B>> functor) {
        Option<B> option2 = option1.flatMap(e1 -> functor.get());
        return new OptionStep2<>(option1, option2);
    }

    public <Z> Option<Z> yield(Function1<A, Z> function1) {
        return option1.map(function1::apply);
    }

}
