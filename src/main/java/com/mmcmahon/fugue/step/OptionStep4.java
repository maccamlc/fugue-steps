package com.mmcmahon.fugue.step;

import com.mmcmahon.fugue.step.functions.Function4;
import com.mmcmahon.fugue.step.ops.OptionStep;
import io.atlassian.fugue.Option;

import java.util.function.Supplier;

public class OptionStep4<A, B, C, D> implements OptionStep {

    private final Option<A> option1;
    private final Option<B> option2;
    private final Option<C> option3;
    private final Option<D> option4;

    OptionStep4(
            Option<A> option1,
            Option<B> option2,
            Option<C> option3,
            Option<D> option4) {
        this.option1 = option1;
        this.option2 = option2;
        this.option3 = option3;
        this.option4 = option4;
    }

    public <E> OptionStep5<A, B, C, D, E> then(Function4<A, B, C, D, Option<E>> functor) {
        Option<E> option5 = option1.flatMap(
                e1 -> option2.flatMap(
                        e2 -> option3.flatMap(
                                e3 -> option4.flatMap(
                                        e4 -> functor.apply(e1, e2, e3, e4)))));
        return new OptionStep5<>(option1, option2, option3, option4, option5);
    }

    public <E> OptionStep5<A, B, C, D, E> then(Supplier<Option<E>> functor) {
        Option<E> option5 = option1.flatMap(
                e1 -> option2.flatMap(
                        e2 -> option3.flatMap(
                                e3 -> option4.flatMap(
                                        e4 -> functor.get()))));
        return new OptionStep5<>(option1, option2, option3, option4, option5);
    }

    public <Z> Option<Z> yield(Function4<A, B, C, D, Z> functor) {
        return option1.flatMap(
                e1 -> option2.flatMap(
                        e2 -> option3.flatMap(
                                e3 -> option4.map(
                                        e4 -> functor.apply(e1, e2, e3, e4)))));
    }

}
