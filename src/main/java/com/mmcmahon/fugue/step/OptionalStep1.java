package com.mmcmahon.fugue.step;

import com.mmcmahon.fugue.step.functions.Function1;
import com.mmcmahon.fugue.step.ops.OptionalStep;

import java.util.Optional;
import java.util.function.Supplier;

@SuppressWarnings("OptionalUsedAsFieldOrParameterType")
public class OptionalStep1<A> implements OptionalStep {

    private final Optional<A> optional1;

    OptionalStep1(Optional<A> optional1) {
        this.optional1 = optional1;
    }

    public <B> OptionalStep2<A, B> then(Function1<A, Optional<B>> functor) {
        Optional<B> option2 = optional1.flatMap(functor::apply);
        return new OptionalStep2<>(optional1, option2);
    }

    public <B> OptionalStep2<A, B> then(Supplier<Optional<B>> functor) {
        Optional<B> Optional = optional1.flatMap(e1 -> functor.get());
        return new OptionalStep2<>(optional1, Optional);
    }

    public <Z> Optional<Z> yield(Function1<A, Z> function1) {
        return optional1.map(function1::apply);
    }

}
