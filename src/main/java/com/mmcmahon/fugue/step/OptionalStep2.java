package com.mmcmahon.fugue.step;

import com.mmcmahon.fugue.step.functions.Function2;
import com.mmcmahon.fugue.step.ops.OptionalStep;

import java.util.Optional;
import java.util.function.Supplier;

@SuppressWarnings("OptionalUsedAsFieldOrParameterType")
public class OptionalStep2<A, B> implements OptionalStep {

    private final Optional<A> optional1;
    private final Optional<B> optional2;

    OptionalStep2(
            Optional<A> optional1,
            Optional<B> optional2) {
        this.optional1 = optional1;
        this.optional2 = optional2;
    }

    public <C> OptionalStep3<A, B, C> then(Function2<A, B, Optional<C>> functor) {
        Optional<C> option3 = optional1.flatMap(e1 -> optional2.flatMap(e2 -> functor.apply(e1, e2)));
        return new OptionalStep3<>(optional1, optional2, option3);
    }

    public <C> OptionalStep3<A, B, C> then(Supplier<Optional<C>> functor) {
        Optional<C> Optional = optional1.flatMap(e1 -> optional2.flatMap(e2 -> functor.get()));
        return new OptionalStep3<>(optional1, optional2, Optional);
    }

    public <Z> Optional<Z> yield(Function2<A, B, Z> function2) {
        return optional1.flatMap(e1 -> optional2.map(e2 -> function2.apply(e1, e2)));
    }

}
