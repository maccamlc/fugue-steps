package com.mmcmahon.fugue.step;

import com.mmcmahon.fugue.step.functions.Function1;
import com.mmcmahon.fugue.step.ops.TryStep;
import io.atlassian.fugue.Try;

import java.util.function.Supplier;

public class TryStep1<E1> implements TryStep {

    private final Try<E1> try1;

    TryStep1(Try<E1> try1) {
        this.try1 = try1;
    }

    public <E2> TryStep2<E1, E2> then(Function1<E1, Try<E2>> function1) {
        Try<E2> try2 = try1.flatMap(function1::apply);
        return new TryStep2<>(try1, try2);
    }

    public <E2> TryStep2<E1, E2> then(Supplier<Try<E2>> supplier) {
        Try<E2> either2 = try1.flatMap(e1 -> supplier.get());
        return new TryStep2<>(try1, either2);
    }

    public <Z> Try<Z> yield(Function1<E1, Z> function1) {
        return try1.map(function1::apply);
    }

}

