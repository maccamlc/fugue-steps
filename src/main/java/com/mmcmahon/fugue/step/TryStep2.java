package com.mmcmahon.fugue.step;

import com.mmcmahon.fugue.step.functions.Function2;
import com.mmcmahon.fugue.step.ops.TryStep;
import io.atlassian.fugue.Try;

import java.util.function.Supplier;

public class TryStep2<A, B> implements TryStep {

    private final Try<A> try1;
    private final Try<B> try2;

    TryStep2(
            Try<A> try1,
            Try<B> try2) {
        this.try1 = try1;
        this.try2 = try2;
    }

    public <C> TryStep3<A, B, C> then(Function2<A, B, Try<C>> functor) {
        Try<C> try3 = try1.flatMap(e1 -> try2.flatMap(e2 -> functor.apply(e1, e2)));
        return new TryStep3<>(try1, try2, try3);
    }

    public <C> TryStep3<A, B, C> then(Supplier<Try<C>> functor) {
        Try<C> Try = try1.flatMap(e1 -> try2.flatMap(e2 -> functor.get()));
        return new TryStep3<>(try1, try2, Try);
    }

    public <Z> Try<Z> yield(Function2<A, B, Z> function2) {
        return try1.flatMap(e1 -> try2.map(e2 -> function2.apply(e1, e2)));
    }

}
