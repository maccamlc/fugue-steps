package com.mmcmahon.fugue.step.functions;

@FunctionalInterface
public interface Function1<T, R> {

    R apply(T t);

}
