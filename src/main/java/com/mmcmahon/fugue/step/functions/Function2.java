package com.mmcmahon.fugue.step.functions;

@FunctionalInterface
public interface Function2<A, B, Z> {

    Z apply(A a, B b);

}
