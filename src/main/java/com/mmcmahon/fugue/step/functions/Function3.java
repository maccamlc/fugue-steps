package com.mmcmahon.fugue.step.functions;

@FunctionalInterface
public interface Function3<T, U, X, R> {

    R apply(T t, U u, X x);

}
