package com.mmcmahon.fugue.step;

public class AnError {

    static class FIRST extends AnError {
        static FIRST ERROR = new FIRST();

        private FIRST() {
        }
    }

    static class SECOND extends AnError {
        static SECOND ERROR = new SECOND();

        private SECOND() {
        }
    }

    static class THIRD extends AnError {
        static THIRD ERROR = new THIRD();

        private THIRD() {
        }
    }

    static class FOURTH extends AnError {
        static FOURTH ERROR = new FOURTH();

        private FOURTH() {
        }
    }

    static class FIFTH extends AnError {
        static FIFTH ERROR = new FIFTH();

        private FIFTH() {
        }
    }

    static class SIXTH extends AnError {
        static SIXTH ERROR = new SIXTH();

        private SIXTH() {
        }
    }

}
